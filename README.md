## After cloning follow the below steps

- composer install
- npm install
- cp .env.example .env
- Now insert the database name etc in .env file
- php artisan migrate:fresh --seed
- php artisan passport:install --force
- php artisan storage:link
- php artisan key:generate


## if you want to run web version then follow below steps
- In one terminal run :  php artisan serve
- In second terminal run :  npm run watch

## To Test api please follow the api documentation and test with    POSTMAN


## if you find any issue please wrote down below
- Example like this

