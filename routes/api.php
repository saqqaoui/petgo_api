<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('API')->group(function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::post('login', 'AuthController@login');
        Route::post('reset-password-request', 'AuthController@resetPasswordRequest');
        Route::post('reset-password', 'AuthController@resetPassword');

        // Route::post('refresh', 'AuthController@refresh')->name("api_refresh");
        // Route::get('activate/{user}', 'AuthController@activate')->name("api_activate");

        Route::group(['middleware' => 'auth:api'], function() {
            Route::get('profile', 'AuthController@profile');
            Route::post('change-password', 'AuthController@changePassword');
            Route::get('logout','AuthController@logout');
        });
    });
    Route::group(['prefix' => 'admin', 'middleware' => 'auth:api'], function () {
        Route::group(['middleware' => 'scope:admin'], function () {
            
        });
    });

    Route::group(['prefix' => 'user', 'middleware' => 'auth:api'], function () {
        Route::group(['middleware' => 'scope:user'], function () {
            Route::get('/{user}/pets', 'UserController@pets');
            Route::post('/{user}/pets/active/{pet}', 'UserController@activepet');
        });
    });

    //api's petgo original
    Route::resource('/users' , 'UserController');
    Route::apiResources([
        //  '/users' => 'UserController',
  //    '/pets' => 'PetController',
      '/posts' => 'PostController',
      '/comments' => 'CommentController',
      '/stories' => 'StoryController',
      '/galleries' => 'GalleryController',
      '/hangouts' => 'HangoutController',
      '/match_announces' => 'MatchAnnounceController',
  ]);

  Route::apiResource('/pets', "PetController")->except("update");

    Route::prefix('/pets')->group(function () {

        Route::post('/{pet}', 'PetController@update');
        Route::post('/{pet}/upload_pic', 'PetController@uploadPictures');
        Route::post('/{pet}/setActive', 'PetController@setActive');
        // Route::post('/{pet}/follow', 'PetController@follow');
        Route::post('/{pet}/like_model', 'PetController@like_model'); // Like Post or Comment

        // Get requests
        Route::get('/{pet}/user', 'PetController@user');
        Route::get('/{pet}/posts', 'PetController@posts');
        Route::get('/{pet}/likes', 'PetController@likes');

        Route::get('/search/{query}', 'PetController@search');

        Route::get('/{pet}/sent_hangouts', 'HangoutController@sent_hangouts');
        Route::get('/{pet}/received_hangouts', 'HangoutController@received_hangouts');
    });


    Route::match(["post", "get"], "/search", 'DefaultController@search');


    Route::prefix('/species')->group(function () {
        Route::get('/', 'PetSpeciesController@species');
        Route::get( '/{species}', 'PetSpeciesController@show');
    });

        // =======================================================================================================
    Route::prefix('/posts')->group(function () {
        Route::post('/{post}/add_comment', 'CommentController@store');
    });
    Route::post('/{pet}/follow', 'PetController@follow');
    // =======================================================================================================
    
    
    // for sign up
    Route::post('/otp-request', 'AuthController@otpRequest');
    Route::post('/activate-user', 'AuthController@userActive');
});
