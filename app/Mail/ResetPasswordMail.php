<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResetPasswordMail extends Mailable
{
    use Queueable, SerializesModels;
    
    protected $user;
    protected $confirmation_code;

    /**
     * ResetPasswordMail constructor.
     * @param $user
     * @param $confirmation_code
     *
     * Create a new message instance.
     *
     * @return void
     */

    public function __construct($user, $confirmation_code)
    {
        $this->user = $user;
        $this->confirmation_code = $confirmation_code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.account.reset_password', [
            'confirmation_code' => $this->confirmation_code,
            'user' => $this->user,
        ])
            ->to($this->user)
            ->subject("Reset Password")
            ;
    }
}
