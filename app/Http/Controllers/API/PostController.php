<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Models\Comment;
use App\Models\Like;
use App\Models\Pet;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Monolog\Formatter\JsonFormatter;


class PostController extends Controller
{
    /**
     * PostController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', "show"]]);
    }
    
    /**
     * Display a listing of the resource.
     * 
     * @return JsonResponse
     *
     */
    public function index()
    {
        $posts = Post::with(["pet", "tagged_pets" => function($q) {
                    return $q->select("name");
                },
            "likes",
            ])
            ->orderBy("created_at", "desc")
            ->paginate(30);
        
        
        return response()->json([
            "posts" => $posts->items(),
            "config" => [
                "last_page" => $posts->lastPage(),
                "per_page" => $posts->perPage(),
                "total" => $posts->total(),
                "current_page" => $posts->currentPage(),
            ]
    
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return string
     */
    public function store(Request $request)
    {
        
        try {
            $pet = Pet::findOrFail($request->pet_id);
            $post = Post::create($request->all());
            
            if ($request->hasFile("mediaLink")) {
                $file = $request->file("mediaLink");
                $name = $pet->id . "-" . Str::slug($pet->name);
                $user_name = $pet->user->id . "-" . Str::slug($pet->user->first_name) . "-" . Str::slug($pet->user->last_name);

                $file->storeAs(
                    "/public/$user_name/pets/$name/posts/",
                    "post-{$post->id}.{$file->getClientOriginalExtension()}"
                );

                $post->media_url = "/storage/$user_name/pets/$name/posts/post-{$post->id}.{$file->getClientOriginalExtension()}";
                $post->save();
            }

//            foreach ($request->tagged_pet_id as $value) {
                $post->tagged_pets()->attach($request->tagged_pet_id);
//            }
            
            return response()->json([
                'post' => $post,
//                'attached' => $request->tagged_pet_id
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }
//        return $request->all();
    }

    
    /**
     * Display the specified resource.
     *
     * @param Post $post
     * @return Post
     */
    public function show(Post $post)
    {
        return $post
            ->load([
                "tagged_pets" => function($q) {
                    return $q->select("name");
                },
                "pet", "likes", "comments"
            ]);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Post $post
     * @return bool
     */
    public function update(Request $request, Post $post)
    {
        try {
            return $post->update($request->all());
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Post $post
     * @return string
     */
    public function destroy(Post $post)
    {
        try {
            return $post->delete();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
    
    
    // ============================================================================

    /**
     * Attach comment to Post.
     *
     * @param Request $request
     * @param Post $post
     * @return string
     */

    public function addComment(Request $request, Post $post)
    {
        try {

            return $post->comments()->create($request->all())->load("pet");

        } catch (\Exception $e) {
            return response()->json([
                "Error" => $e->getMessage()
            ]);
        }
    }


    /**
     * Attach comment to Post.
     *
     * @param Pet $pet
     * @param Post $post
     * @return string
     */

}
