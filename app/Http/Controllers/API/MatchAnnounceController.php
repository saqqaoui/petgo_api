<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Models\MatchAnnounce;
use App\Models\Pet;
use App\Models\User;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Psy\Util\Json;

class MatchAnnounceController extends Controller
{
    /**
     * MatchAnnounceController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', "show"]]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return string
     */
    public function index()
    {
//        return MatchAnnounce::with(["pet" => function($q){
//            return $q->select( "name", "user_id");
//        }])->get();
        return MatchAnnounce::with(["pet" => function($q){
            return $q->with(["user", "breed"])
                ->select( ["id", "name", "user_id", "breed_id"]);
        }
        ])->get();
        
//        return response()->json([
//            "matches" => $matchs->get()
//        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return string
     */
    public function store(Request $request)
    {
        try {
            $match = MatchAnnounce::create($request->all());
            $pet = $match->pet;

            $file = $request->file("picture_url");
            $pet_name = $pet->id . "-" . Str::slug($pet->name);
            $user_name = $pet->user->id . "-" . Str::slug($pet->user->first_name) . "-" . Str::slug($pet->user->last_name);

            $file->storeAs(
                "/public/$user_name/pets/$pet_name/matches/",
                "match-{$match->id}.{$file->getClientOriginalExtension()}"
            );

            $match->picture_url = "/storage/$user_name/pets/$pet_name/matches/match-{$match->id}.{$file->getClientOriginalExtension()}";
            $match->save();

            return response()->json([
                "status" => "success",
                "match" => $match,
            ]);
            
        } catch (Exception $e) {
            return response()->json([
                "status" => "fail",
                "message" => $e->getMessage(),
            ]);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param MatchAnnounce $matchAnnounce
     * @return MatchAnnounce
     */
    public function show(MatchAnnounce $matchAnnounce)
    {
        return $matchAnnounce->load(["pet" => function($q){
            return $q->with(["user", "breed"])
                ->select( ["id", "name", "user_id", "breed_id", "pic_url", "cover_url", "about_me"]);
        }]);
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param MatchAnnounce $matchAnnounce
     * @return string
     */
    public function update(Request $request, MatchAnnounce $matchAnnounce)
    {
        try {
            $match = $matchAnnounce->update($request->all);
            return Json::encode([
                "match" => $match
            ]);
        } catch (Exception $e) {
            return Json::encode([
                "Error" => $e->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param MatchAnnounce $matchAnnounce
     * @return string
     */
    public function destroy(MatchAnnounce $matchAnnounce)
    {
        try {
            $matchAnnounce->delete();
            return Json::encode([
               "status" => "success",
                "message" => "Announce Deleted success"
            ]);
        } catch (Exception $e) {
            return Json::encode([
                "status" => "success",
                "message" => $e->getMessage()
            ]);
        }
    }
}
