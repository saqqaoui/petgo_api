<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Models\Hangout;
use App\Models\Pet;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;
use Psy\Util\Json;

class HangoutController extends Controller
{
    /**
     * PostController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', "show", "sent_hangouts", "received_hangouts"]]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return string
     */
    public function index()
    {
        try {
            return Hangout::with(
                [
                    "pet",
                    "participants" => function($q){
                        return $q->select( ["name", "pic_url"]);
                    }
                ])
                ->public( request()->pet_id )
                ->active()
                ->orderBy("date_time", 'asc')
                ->get();
        }
        catch (\Exception $exception) {
            return response()->json([
                "Error" => $exception->getMessage()
            ]);
        }
//        return Carbon::now();
    }


    /**
     *
     * Sent hangouts
     * @param Pet $pet
     * @return JsonResponse
     */
    public function sent_hangouts(Pet $pet){
        try {
            return Hangout::with(
                [
                    "pet",
                    "participants" => function($q){
                        return $q->select( ["name", "pic_url"]);
                    }
                ])
                ->sent( $pet->id )
                ->active()
                ->orderBy("date_time", 'asc')
                ->get();
        }
        catch (\Exception $exception) {
            return response()->json([
                "Error" => $exception->getMessage()
            ]);
        }
    }

    /**
     *
     * Received hangouts
     * @param Pet $pet
     * @return JsonResponse
     */
    public function received_hangouts(Pet $pet){
        try {
            return $pet->received_hangouts;
        }
        catch (\Exception $exception) {
            return response()->json([
                "Error" => $exception->getMessage()
            ]);
        }
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return string
     */
    public function store(Request $request)
    {
        try {
            $pet = Pet::find($request->pet_id);

            $hangout = Hangout::create($request->all());

            $name = $pet->id . "-" . Str::slug($pet->name);
            $user_name = $pet->user->id . "-" . Str::slug($pet->user->first_name) . " " . Str::slug($pet->user->last_name);
            //hangouts

            $req_pic = $request->file("hangout_image");
            $path = "/public/$user_name/pets/$name/hangouts";
            $storage_path = "/storage/$user_name/pets/$name/hangouts";
            $filename = "hangout-{$hangout->id}.{$req_pic->getClientOriginalExtension()}";
            $req_pic->storeAs($path, $filename);

            $hangout->hangout_image = "$storage_path/$filename";
            $hangout->save();

            $invited_pet = Pet::find($request->invited_pet_id);
            $hangout->participants()->attach($invited_pet);

            return $hangout->load(["pet", "participants" => function($q) {
                return $q->select(["name"]);
            }]);

        }
        catch (\Exception $exception) {
            return Json::encode([
                "Error" => $exception->getMessage()
            ]);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  Hangout  $hangout
     * @return string
     */
    public function show(Hangout $hangout)
    {
        try {
            return Json::encode([
                "status" => "success",
                "hangout" => $hangout
            ]);
//            return $hangout;
        }
        catch (\Exception $exception) {
            return Json::encode([
                "status" => "failed",
                "Error" => $exception->getMessage()
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Hangout $hangout
     * @return JsonResponse
     */
    public function update(Request $request, Hangout $hangout)
    {

        try {
            $participant = $hangout->participants->find($request->pet_id);

            if ($participant) {
                $participant->pivot->update(["decision" => $request->decision]);

                return response()->json([
                    'status' => "success",
                    'response' => $participant->pivot,
                ]);
            }

            else {
                return response()->json([
                    'status' => 'failed',
                    "Error" => "Pet not found"
                ]);
            }

        } catch (\Exception $exception) {
            return response()->json([
                'response' => 'failed',
                "Error" => $exception->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Hangout $hangout
     * @return bool
     */
    public function destroy(Hangout $hangout)
    {
        try {
            return $hangout->delete();
        }
        catch (\Exception $exception) {
            return Json::encode([
                "Error" => $exception->getMessage()
            ]);
        }
    }
}
