<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Mail\AccountActivationMail;
use App\Models\Pet;
use App\Models\PetSpecies;
use App\Models\User;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class UserController extends Controller
{

    public function __construct()
    {
//        $this->middleware('auth:api', ['only' => ['update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return User[]|Collection
     */
    public function index()
    {
        return User::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $new_user = User::create($request->all());

            \MultiMail::to($new_user)
                ->from('noreply@petgo.website')->send(new AccountActivationMail($new_user));
//            Mail::send(new AccountActivationMail($new_user));

            $credentials = $request->all(['email', 'password']);

            if (!$token = auth()->attempt($credentials)) {
                return response()->json([
                    "status" => "failed",
                ]);
            }

            return response()->json([
                'status' => 'success',
                "token" => $token,
                "user" => $new_user->only(["id", "first_name", "last_name", "email", "reg_code", "dep_code", "city_code", "city_name", "pro", "active_pet", "active"])
            ]);
        }
        catch (\PDOException $e){

            return response()->json([
                'status' => 'fail',
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return string
     */
    public function show(User $user)
    {
        try {
            $users = User::with('pets')->get();
            return response()->json($users, 200);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     */
    public function update(Request $request, User $user)
    {
        try {
            if ($request->has("active_pet")) {
                $user->update([
                    "active_pet" => $request->active_pet
                ]);

                $active_pet = $user->pets->find( $user->active_pet );
                $pet = $active_pet
                    ->load([
                        "breed" => function($bq){
                            return $bq->with(["species" => function($sq){
                                return $sq->select("id", "name_fr", "name_en");
                            }
                            ])
                                ->select("id", "name_fr", "name_en", "pet_species_id");
                        }])
                    ->makeHidden([
                        "deleted_at", "created_at", "updated_at",
                        "vaccine_date", "species_id", "all_posts"
                    ]);
                return response()->json([
                    "status" => "success",
                    "pet" => $pet
                ]);

            } else {
                $user->update($request->all());
                $user->password = bcrypt($user->password);
                $user->activation_token = Str::random(60);
                $user->save();

                \MultiMail::to($user)
                    ->from('noreply@petgo.website')->send(new AccountActivationMail($user));

                $response  = response()->json([
                    "status" => "success",
                    "user" => $user->only(["id", "first_name", "last_name", "email", "reg_code", "dep_code", "city_code", "city_name", "pro", "active_pet"]),
                ]);
            }

            return $response;
        } catch (Exception $e) {
            return response()->json([
                "status" => "failed",
                "result" => $e->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return void
     * @throws Exception
     */
    public function destroy(User $user)
    {
        $user->delete();
    }


    // Relations ======================================
    public function pets(User $user)
    {
        return $user->pets->makeHidden("all_posts");
//        return $user->pets->makeHidden(['user_id', 'breed_id', "species_id", 'birthday', "genre",
//            "vaccine", "vaccine_date", "about_me", "cover_url", "deleted_at", "created_at", 'updated_at', "all_posts"]);
    }

    public function setActivePet(User $user, Pet $pet)
    {
        $user->active_pet = $pet->id;
        $user->save();
    }




}
