<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Mail\ResetPasswordMail;
use App\Models\Pet;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Psy\Util\Json;
use Spatie\Searchable\Search;

class DefaultController extends Controller
{


    public function search(Request $request)
    {
        $result = (new Search())
            ->registerModel(Pet::class, "name")
//        ->registerModel(Post::class, "body")
            ->search( $request->text );

        return Json::encode(["result" => $result]);
    }
    
}
