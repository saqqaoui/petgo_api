<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Models\PetSpecies;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Response;
use Psy\Util\Json;

class PetSpeciesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return string
     */
    public function species()
    {
//        return PetSpecies::where("pet_species_id", null)->get();
        return Json::encode([
            'species' => PetSpecies::where("pet_species_id", null)->get()
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $species
     * @return Builder|Builder[]|Collection|Model|Response
     */
    public function show($species)
    {
        return PetSpecies::with("breeds")->find($species);
    }
}
