<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Models\Comment;
use App\Models\Pet;
use App\Models\Post;
use Illuminate\Http\Request;
use Psy\Util\Json;

class CommentController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @return string
     */
    public function index()
    {
        return Comment::all();
    }
    
    public function store(Request $request, Post $post)
    {
       
        try {
            $comment = new Comment();
            $comment->pet_id = $post->pet_id;
            $comment->post_id = $post->id;
            $comment->body = $request->body;
            if($comment->save()){
                return response()->json($comment, 200);
            }
            return $post->comments()->create($request->all())->load("pet");

        } catch (\Exception $e) {
            return Json::encode([
                "Error" => $e->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Comment $comment
     * @return string
     */
    public function show( Comment $comment)
    {
        try {

            return $comment->load("pet");

        } catch (\Exception $e) {
            return Json::encode([
                "Error" => $e->getMessage()
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Comment $comment
     * @return bool
     */
    public function update(Request $request, Comment $comment)
    {
        return $comment->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Comment $comment
     * @return void
     * @throws \Exception
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();
    }
    
    
    // =====================
    /**
     * Attach comment to Post.
     *
     * @param Pet $pet
     * @param Comment $comment
     * @return string
     */

    public function addLike(Pet $pet, Comment $comment)
    {
        try {
            if(!$comment->likes->contains("pet_id", $pet->id)){
                $comment->likes()->create(["pet_id" => $pet->id, "pet_name" => $pet->name]);
            }
            else {
                $comment->likes()->where("pet_id","$pet->id")->first()->delete();
            }

            return Json::encode([
                "comment" => $comment,
            ]);

        } catch (\Exception $e) {
            return Json::encode([
                "Error" => $e->getMessage()
            ]);
        }
    }
}
