<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Gallery;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Psy\Util\Json;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Gallery[]|Collection
     */
    public function index()
    {
        return Gallery::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return string
     */
    public function store(Request $request)
    {
        try {
            return Gallery::create($request->all());
        }
        catch (\Exception $exception){
            return Json::encode([
                "exception" => $exception->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Gallery $gallery
     * @return Gallery
     */
    public function show(Gallery $gallery)
    {
        return $gallery;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Gallery $gallery
     * @return bool
     */
    public function update(Request $request, Gallery $gallery)
    {
        try {
            return Gallery::update($request->all());
        }
        catch (\Exception $exception){
            return Json::encode([
                "exception" => $exception->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Gallery $gallery
     * @return bool
     */
    public function destroy(Gallery $gallery)
    {
        try {
            return $gallery->delete();
        }
        catch (\Exception $exception){
            return Json::encode([
                "exception" => $exception->getMessage()
            ]);
        }
    }
}
