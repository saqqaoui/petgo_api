<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Models\Comment;
use App\Models\Pet;
use App\Models\PetSpecies;
use App\Models\Post;
use App\Models\Like;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Jenssegers\Agent\Agent;
use Psy\Util\Json;

class PetController extends Controller
{
    /**
     * PetController constructor.
     */
    // public function __construct()
    // {
    //     $this->middleware('auth:api', ['only' => ['store', 'update', "destroy", "forceDestroy", "uploadPictures", 'upload_pic', "setActive", 'follow', 'like']]);
    // }


    /**
     * Display a listing of the resource.
     *
     * @return Pet[]|bool|Collection|int|string
     */
    public function index()
    {
        return Pet::with('user')->get();
//        $agent = new Agent();
//        return  $agent->isAndroidOS();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return string
     */
    public function store(Request $request)
    {

        try {
            $pet = Pet::create($request->all());

            // Upload picture -----------------------------------------
            $this->uploadPictures($pet, $request);
            // ------------------------------------------------------------------------------------------
            $pet->user->update(["active_pet" => $pet->id]);

            return response()->json([
//                "token" => $new_token,
                "pet" => $pet
                    ->makeHidden("user")
                    ->load([
                        "breed" => function($bq){
                            return $bq->with(["species" => function($sq){
                                return $sq->select("id", "name_fr", "name_en");
                            }
                            ])
                                ->select("id", "name_fr", "name_en", "pet_species_id");
                        }])
                    ->makeHidden([
                        "deleted_at", "created_at", "updated_at",
                        "vaccine_date", "species_id", "all_posts"
                    ]),
            ]);

        } catch (\Exception $e) {
            return response()->json([
                "error_message" => $e->getMessage()
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param Pet $pet
     * @return string
     */
    public function show(Pet $pet)
    {
        try {
            if ($pet) {

                return response()->json([
                    "pet" => $pet->load(
                        [
                            'user' => function($q) {return $q->select("id", 'city_name');},
                            'following', 'followers',
                            "breed" => function($bq){ return $bq->with(
                                [
                                    "species" => function($sq){ return $sq->select("id", "name_fr", "name_en"); }
                                ]
                            )->select("id", "name_fr", "name_en", "pet_species_id"); },
                        ]
                    )->makeHidden([
                            "deleted_at", "created_at", "updated_at",
                        "vaccine_date", "species_id"
                        ]),

                    "posts" => $pet->posts
                        ->load(
                        ['tagged_pets' => function($q){
                                return $q->select("name");
                            },
                        ]
                    )
                        ->merge($pet->tag_posts)
                        ->sortBy('created_at')
                        ->flatten(),
                ]);
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'pet not found'
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage()
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Pet $pet
     * @return JsonResponse
     */
    public function update(Request $request, Pet $pet)
    {
        // return $pet->all();
        try {
            
            
            $pet->update($request->all());
            // Upload picture -----------------------------------------
            $this->uploadPictures($pet, $request);
            // ------------------------------------------------------------------------------------------
            $like = Like::where('pet_id' , $pet->id)->first();
            $like->pet_name = $pet->name;
            $like->save();
            
            return response()->json([
//                "token" => $new_token,
                "pet" => $pet
                    ->makeHidden("user")
                    ->load([
                        "breed" => function($bq){
                            return $bq->with(["species" => function($sq){
                                return $sq->select("id", "name_fr", "name_en");
                            }
                            ])
                                ->select("id", "name_fr", "name_en", "pet_species_id");
                        }])
                    ->makeHidden([
                        "deleted_at", "created_at", "updated_at",
                        "vaccine_date", "species_id", "all_posts"
                    ]),
            ]);

        } catch (\Exception $e) {
            return response()->json([
                "error_message" => $e->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Pet $pet
     * @return void
     * @throws \Exception
     */
    public function destroy(Pet $pet)
    {
        $pet->delete();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Pet $pet
     * @return void
     * @throws \Exception
     */
    public function forceDestroy(Pet $pet)
    {
        $pet->forceDelete();
    }


    // Relations ======================================
    public function user(Pet $pet)
    {
        return $pet->user;
    }

    public function posts(Pet $pet)
    {
        return $pet->posts;
    }

    // Other methods ======================================

    protected function uploadPictures(Pet $pet, Request $request){

        $name = $pet->id . "-" . Str::slug($pet->name);
        $user_name = $pet->user->id . "-" . Str::slug($pet->user->first_name) . "-" . Str::slug($pet->user->last_name);


        if ($request->hasFile("profile_pic")) {
            $profile_pic = $request->file("profile_pic");
            $profile_pic->storeAs(
            //            "/public/user_name/pets/$name/",
                "/public/$user_name/pets/$name/",
                "profile_picture.{$profile_pic->getClientOriginalExtension()}"
            );
            $pet->pic_url = "/storage/$user_name/pets/$name/profile_picture.{$profile_pic->getClientOriginalExtension()}";
        }

        if ($request->hasFile("profile_cover")) {
            $profile_cover = $request->file("profile_cover");
            $profile_cover->storeAs(
                "/public/$user_name/pets/$name/",
                "profile_cover.{$profile_cover->getClientOriginalExtension()}"
            );
            $pet->cover_url = "/storage/$user_name/pets/$name/profile_cover.{$profile_cover->getClientOriginalExtension()}";
        }


        $pet->save();

        $pet->user->active_pet = $pet->id;
        $pet->user->save();
    }


    public function setActive(Pet $pet)
    {
        $pet->user->active_pet = $pet->id;
        $pet->user->save();
    }

    public function search($query)
    {
        if ($query) {
            return Pet::where("name", "like", "{$query}%")->get(['id', 'name', 'pic_url']);
        }
        else return Pet::all();
    }



    // Relations methods ======================================
    public function follow(Request $request, Pet $pet)
    {
        try {
            $pet_to_follow = Pet::find($request->pet_to_follow_id);

            if(!$pet->following->contains($pet_to_follow)) {
            $pet->following()->attach($pet_to_follow->id);
            }

            else if($pet->following->contains($pet_to_follow)) {
            $pet->following()->detach($pet_to_follow->id);
            }
            
            return response()->json([
                'pet_name' => $pet_to_follow->name,
                'followers' => $pet_to_follow->followers()->count(),
                'following' => $pet_to_follow->following()->count(),
            ]);
        }

        catch (\Exception $e) {
            return response()->json([
                'err' => $e->getMessage()
            ]);
        }
    }


    public function likes(Pet $pet)
    {
        return $pet->likes;
    }

    public function like_model(Request $request, Pet $pet)
    {
        $model = $request->model_type == "post" ?
            Post::find($request->model_id) :
            Comment::find($request->model_id);

        try {
            if(!$model->likes->contains("pet_id", $pet->id)){
                $model->likes()->create(["pet_id" => $pet->id, "pet_name" => $pet->name]);
            }
            else {
                $model->likes()->where("pet_id", $pet->id)->first()->delete();
            }

            return response()->json([
                "is_liked" => !$model->likes->contains("pet_id", $pet->id),
                "likes_count" => $model->likes()->count(),
            ]);

        } catch (\Exception $e) {
            return response()->json([
                "Error" => $e->getMessage()
            ]);
        }
    }
    // Hangouts ======================================================

    public function my_hangouts(Pet $pet)
    {
        return $pet->my_hangouts;
    }

    public function received_hangouts(Pet $pet)
    {
        return $pet->received_hangouts;
    }
}
