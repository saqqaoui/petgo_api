<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;

class AccountActivationMail extends Mailable
{
    use Queueable, SerializesModels;
    public $user;

    /**
     * Create a new message instance.
     *
     * @param $user
     */
    public function __construct($user)
    {
        $this->user = $user;
        App::setLocale($user->locale_language);
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.account.activation', [
            'user' => $this->user
        ])
            ->locale($this->user->locale_language)
//            ->from('noreply@petgo.website', "PetGo")
//            ->to($this->user->email, "{$this->user->first_name} {$this->user->last_name}")
            ->subject( trans('auth.register.activation_subj') )
            ;
    }
}
