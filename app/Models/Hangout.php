<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer pet_id
 * @property string status
 * @property string hangout_image
 * @property string hangout_type
 * @property string location_name
 * @property double location_lat
 * @property double location_long
 * @property double destination_lat
 * @property double destination_long
 * @property mixed date_time
 * Relationships ================================
 * @property mixed pet
 * @property mixed participants
 */

class Hangout extends Model
{
    use SoftDeletes;

    protected $fillable = [
        "hangout_type", "location_name", "location_long", "pet_id",
        "location_lat", 'destination_long', 'destination_lat', 'date_time',
        'status'
    ];

    protected $hidden = ['pivot','created_at', 'updated_at', 'deleted_at'];


    public function pet()
    {
        return $this->belongsTo(Pet::class)->select("id", "name", "pic_url");
    }

    public function participants()
    {
        return $this->belongsToMany(Pet::class)
            ->withPivot(["selected_polyline_route", "decision"])
            ;
    }

    // Scopes ======================================================================

    public function scopePublic($query, $pet_id){
        $query
            ->where([
                ["hangout_type", "public"],
                ["pet_id", "<>", $pet_id]
            ])
        ;
    }

    public function scopeSent($query, $pet_id){
        $query
            ->where([
                ["pet_id", $pet_id]
            ])
        ;
    }

    public function scopeActive($query){
        $query->where("date_time", ">", Carbon::now());
    }

}
