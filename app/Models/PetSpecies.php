<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PetSpecies extends Model
{
    protected $fillable = ['name_fr', 'name_en', 'type'];

    public function breeds()
    {
        return $this->hasMany(PetSpecies::class);
    }

    public function species()
    {
        return $this->belongsTo(PetSpecies::class, 'pet_species_id');
    }

    public function pets()
    {
        return $this->hasMany(Pet::class);
    }
}
