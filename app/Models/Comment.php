<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;
    protected $hidden = ["commentable_id", "commentable_type", "updated_at", "pet_id", "created_at", "updated_at", "deleted_at"];

    protected $appends = ['comment_timestamp', 'likes_count'];
    protected $fillable = ["pet_id", "body"];
    protected $touches = ['post'];
    
    /**
     * Get the owning commentable model.
     */
//    public function commentable()
//    {
//        return $this->morphTo();
//    }

    public function pet()
    {
        return $this->belongsTo(Pet::class)->select(["id", "name", "pic_url"])->without("all_posts");
    }

    
    public function likes()
    {
        return $this->morphMany(Like::class, "likeable");
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    // Accessors =====================================

    public function getCommentTimestampAttribute()
    {
        return $this->created_at->format("Y-m-d H:i:s");
    }

    public function getLikesCountAttribute()
    {
        return $this->likes->count();
    }

//    public function getLikedByAttribute()
//    {
//        $likedby = [];
//
//        foreach ($this->likes as $like){
//            $likedby[] = $like->pet->id;
//        }
//        return $likedby;
//    }
    
}
