<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $hidden = ["pet", "likeable_id", "likeable_type"];

    protected $fillable = ["pet_id", "pet_name"];

    public function likeable()
    {
        $this->morphTo();
    }


    public function pet()
    {
        return $this->belongsTo(Pet::class);
    }
}
