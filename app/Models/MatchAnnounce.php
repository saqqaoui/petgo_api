<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MatchAnnounce extends Model
{
    protected $fillable = ["pet_id", "announce_body", "reason"];
    protected $appends = ['post_timestamp'];
    protected $hidden = ["created_at", "updated_at"];

    public function pet()
    {
        return $this->belongsTo(Pet::class)
//            ->with(["user"])
//            ->select( ["id", "name", "user_id"])
            ;
    }

    // Accessors =====================================

    public function getPostTimestampAttribute()
    {
        return $this->created_at->format("Y-m-d H:i:s");
    }
}
