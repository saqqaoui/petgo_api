<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Preference extends Model
{
    protected $casts = [
        "species" => "array"
    ];
    
    public function pet()
    {
        return $this->belongsTo(Pet::class);
    }
}