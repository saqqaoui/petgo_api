<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Story extends Model
{
    public $timestamps = false;

    public function storyable()
    {
        return $this->morphTo();
    }
}
