<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

/**
 * @property mixed my_hangouts
 * @property mixed received_hangouts
 * @property integer pet_species_id
 */
class Pet extends Model implements Searchable
{

    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'name', 'species_id', "breed_id", 'birthday', 
        'genre', 'vaccine', 'about_me'
//        , 'preferences'
        
    ];
    
    protected $hidden = ['posts', 'tag_posts'];
    
//    protected $with = ['user'];
//    protected $hidden = ['posts'];
    // Relations =====================================

    public function user()
    {
        return $this->belongsTo(User::class)
            ->select(['id', 'city_name', "city_code", 'first_name', 'last_name'])
            ;
    }
    
    public function breed()
    {
        return $this->belongsTo(PetSpecies::class, "breed_id");
    }
    
    
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function tag_posts()
    {
        return $this->belongsToMany(Post::class, "pet_post", "pet_id", "post_id");
    }

    /**
     * Get all of the post's comments.
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * Get all of the likes.
     */
    public function likes()
    {
        return $this->hasMany(Like::class);
    }
    
    /**
     * Get all of the galleries.
     */
    public function galleries()
    {
        return $this->hasMany(Gallery::class);
    }
    
    /**
     * Get preference.
     */
    public function preference()
    {
        return $this->hasOne(Preference::class);
    }

    /**
     * Get all of the match announces.
     */
    public function matchAnnounces()
    {
        return $this->hasMany(MatchAnnounce::class);
    }
    
    
    // Special relations =============================

    public function following()
    {
        return $this->belongsToMany(Pet::class, "followers_table", "active_pet_id", "following_ids");
    }

    public function followers()
    {
        return $this->belongsToMany(Pet::class, "followers_table", "following_ids", "active_pet_id");
    }
    
    // hangouts =============================

    public function received_hangouts()
    {
        return $this->belongsToMany(Hangout::class)
            
            ->with([
                "participants" => function($q) {
                    return $q->select( ["name", "pic_url"]);
                },
                "pet"
            ])
            ->where("hangout_type", "private")
            ->whereDate('date_time','>', Carbon::now())
            ->wherePivot("decision", null)
            ->wherePivot("selected_polyline_route", null)
            ->orderBy("date_time", "desc")
            ;
    }
    
    public function my_hangouts()
    {
        return $this->hasMany(Hangout::class)
//            ->where("hangout_type", "private")
//            ->select([
//                "participants" => function($q) {
//                    return $q->select("name", "id");
//                }
//            ])
            ->whereDate('date_time','>', Carbon::now())
            ->orderBy("date_time", "asc");
    }

    // Accessors =====================================
    
    public function getBreedAttribute()
    {
        return $this->breed();
    }

    public function getPostsCounterAttribute()
    {
        return $this->posts->count();
    }
    
    // Mutations =====================================

    // Scopes ========================================
    public function getSearchResult(): SearchResult
    {
        $url = route("pets.show", $this->id);

        return new SearchResult(
            $this,
            $this->name,
            $url
        );
    }
    
}
