import Vue from 'vue';
import Router from 'vue-router';
import * as auth from './services/auth_service';
import store from './store';

Vue.use(Router);

const routes = [
    {
        path: '/',
        name: 'login',
        component: () => import('./views/dashboard/Authentication.vue'),
    },
    {
        path: '/change-password',
        name: 'change-password',
        component: () => import('./views/dashboard/ChangePassword.vue'),
    },
    {
        path: '/admin',
        name: 'dashboard',
        component: () => import('./views/dashboard/Home.vue'),
        children: [
            {
                path: '',
                name: 'dashboard',
                component: () => import('./views/dashboard/admin/Dashboard.vue')
            },
            {
                path: "all-users",
                name: "all-users",
                component: () => import('./views/dashboard/User/AllUsersComponent.vue'),
            },
            {
                path: "add-user",
                name: "add-user",
                component: () => import('./views/dashboard/User/AddUserComponent.vue'),
            },
            {
                path: "pets/:petId",
                name: "pet-infos",
                component: () => import('./views/dashboard/Pet/PetInfosComponent.vue'),
            },
            {
                path: "add-pet",
                name: "add-pet",
                component: () => import('./views/dashboard/Pet/AddPetComponent.vue'),
            },
            {
                path: "all-pets",
                name: "all-pets",
                component: () => import('./views/dashboard/Pet/AllPetsComponent.vue'),
            },
            {
                path: "chat",
                name: "chat",
                component: () => import('./views/dashboard/Chat/ChatComponent.vue'),
            },
            {
                path: "app-component",
                name: "app-component",
                component: () => import('./components/AppComponnent'),
            },
        ],
        beforeEnter(to, from, next) {
            if (auth.isLoggedIn() && auth.getUserRole() === 'admin') {
                next();
            } else {
                next('/404');
            }
        }
    },
    {
        path: '/user',
        name: 'dashboard',
        component: () => import('./views/dashboard/Home.vue'),
        children: [
            {
                path: '',
                name: 'dashboard',
                component: () => import('./views/dashboard/user/Dashboard.vue')
            },
        ],
        beforeEnter(to, from, next) {
            if (auth.isLoggedIn() && auth.getUserRole() === 'user') {
                next();
            } else {
                next('/login');
            }
        }
    },
    {
        path: '*',
        name: '404',
        component: () => import('./views/404.vue')
    }
];

const router = new Router({
    mode: 'history',
    routes: routes,
    linkActiveClass: 'active'
});

router.beforeResolve((to, from, next) => {
    store.state.mainMenuStatus = false;
    next();
});

router.afterEach((to, from) => {
    store.state.mainMenuStatus = false;
});

export default router;
