<!DOCTYPE html>
<html>

<head>
    <title>OTP Request</title>
    
    <style>
        body {
            margin: 0;
            padding: 0;
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
            background: #edf2ff;
        }
    </style>

</head>

<body marginwidth="0" marginheight="0" style=" background-color:black; margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;" offset="0" topmargin="0" leftmargin="0">
    <center style="margin-top:50px;">
        <img src="{{ asset('assets/images/fulllogo.png') }}" style="width: 200px;">
        <h5 style="color:white;">Please Enter the Code Given Below in your App to Activate Your Account.</h5>
        <h1 style="color:white;">{{$otp}}</h1>
    </center>
</body>
</html>
